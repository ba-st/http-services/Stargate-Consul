# Stargate Consul

![Logo](assets/logo.svg)

This library provides a [Stargate](https://github.com/ba-st/Stargate) operational
plugin to interact with the [Consul](https://www.consul.io) HTTP API.

[![Unit Tests](https://github.com/ba-st/Stargate-Consul/actions/workflows/unit-tests.yml/badge.svg)](https://github.com/ba-st/Stargate-Consul/actions/workflows/unit-tests.yml)
[![Coverage Status](https://codecov.io/github/ba-st/Stargate-Consul/coverage.svg?branch=release-candidate)](https://codecov.io/gh/ba-st/Stargate-Consul/branch/release-candidate)
[![Baseline Groups](https://github.com/ba-st/Stargate-Consul/actions/workflows/loading-groups.yml/badge.svg)](https://github.com/ba-st/Stargate-Consul/actions/workflows/loading-groups.yml)
[![Markdown Lint](https://github.com/ba-st/Stargate-Consul/actions/workflows/markdown-lint.yml/badge.svg)](https://github.com/ba-st/Stargate-Consul/actions/workflows/markdown-lint.yml)
[![Shellcheck](https://github.com/ba-st/Stargate-Consul/actions/workflows/shellcheck.yml/badge.svg)](https://github.com/ba-st/Stargate-Consul/actions/workflows/shellcheck.yml)

[![GitHub release](https://img.shields.io/github/release/ba-st/Stargate-Consul.svg)](https://github.com/ba-st/Stargate-Consul/releases/latest)
[![Pharo 8.0](https://img.shields.io/badge/Pharo-8.0-informational)](https://pharo.org)
[![Pharo 9.0](https://img.shields.io/badge/Pharo-9.0-informational)](https://pharo.org)
[![Pharo 10](https://img.shields.io/badge/Pharo-10-informational)](https://pharo.org)

Quick links

- [**Explore the docs**](docs/README.md)
- [Report a defect](https://github.com/ba-st/Stargate-Consul/issues/new?labels=Type%3A+Defect)
- [Request a feature](https://github.com/ba-st/Stargate-Consul/issues/new?labels=Type%3A+Feature)

The integration provided in the operational plugin interacts with Consul HTTP
API to register and deregister the configured services when the API starts/stops.

## License

- The code is licensed under [MIT](LICENSE).
- The documentation is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/).

## Installation

To load the project in a Pharo image follow this [instructions](docs/how-to/how-to-load-in-pharo.md).

## Contributing

Check the [Contribution Guidelines](CONTRIBUTING.md)
